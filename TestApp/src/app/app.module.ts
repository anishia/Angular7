import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DatabaseCmpComponent } from './database-cmp/database-cmp.component';
import { UIToolsComponent } from './uitools/uitools.component';
import { PipeComponent } from './pipe/pipe.component';

@NgModule({
  declarations: [    
    DatabaseCmpComponent,
    UIToolsComponent,
    PipeComponent
  ],
  entryComponents: [DatabaseCmpComponent, UIToolsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: []
})

export class AppModule {
  ngDoBootstrap(app) {
    fetch('url/to/fetch/component/name')
      .then((name)=>{ bootstrapRootComponent(app, name)});
  }

 
 }

  // app - reference to the running application (ApplicationRef)
// name - name (selector) of the component to bootstrap
function bootstrapRootComponent(app, name) {
  // define the possible bootstrap components 
  // with their selectors (html host elements)
  const options = {
    'app-database-cmp': DatabaseCmpComponent,
    'app-uitools': UIToolsComponent
    
  };
  // obtain reference to the DOM element that shows status
  // and change the status to `Loaded`
  const statusElement = document.querySelector('#status');
  alert(name);
  statusElement.textContent = 'Loaded';
  // create DOM element for the component being bootstrapped
  // and add it to the DOM
  const componentElement = document.createElement(name);
  document.body.appendChild(componentElement);
  // bootstrap the application with the selected component
  const component = options[name];
  app.bootstrap(component);
}
function fetch(url) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('app-uitools');
    }, 2000);
  });
}

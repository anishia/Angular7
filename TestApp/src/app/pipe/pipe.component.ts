import { Component, OnInit,Pipe,PipeTransform } from '@angular/core';

@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrls: ['./pipe.component.css']
})
export class PipeComponent implements OnInit,PipeTransform {
  transform(value:string):string{
    return value.split('').reverse().join('');
  }
  name:string="";
  constructor() { 
    this.name="Test Name";
    alert(this.name);
  }

  ngOnInit(): void {
  }

}

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TestApp';
  dSalary = 10.263782;
  sStr: string = 'Center For Technology Excellence';
  objDate = new Date(2019,5,19,5,42,49,49);  
}
